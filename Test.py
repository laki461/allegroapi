## Author: LukaszKoziara

import sys
import unittest
import base64
import requests


class AllegroTest(unittest.TestCase):
    API_URL_AUTH = "https://allegro.pl/auth/oauth/token?grant_type=client_credentials"
    API_URL = "https://api.allegro.pl"
    TOKEN = ""
    CLIENT = ""
    SECRET = ""
    HEADER = {}

    @classmethod
    def setUp(self):
        pwd = self.CLIENT + ":" + self.SECRET
        b64Val = base64.b64encode(pwd.encode()).decode()  # workaround for python3
        res = requests.post(self.API_URL_AUTH,
                            headers={"Authorization": "Basic %s" % b64Val})
        self.TOKEN = res.json().get("access_token")
        # self.assertNotEqual(self.TOKEN,"", "Token collected!")
        self.HEADER = {"Accept": "application/vnd.allegro.public.v1+json",
                       "Accept-Language": "pl-PL", "Authorization": "Bearer " + self.TOKEN}

    def test_01_getCategoriesPosTest(self):
        res = requests.get(self.API_URL + "/sale/categories", headers=self.HEADER)
        data = res.json()
        for cat in data['categories']:
            catByID = self.getCategoryByID(cat['id']).json()
            catByIDPar = self.getParametersByCat(cat['id']).json()
            bRv = False
            if cat == catByID:
                for par in catByIDPar['parameters']:
                    if par['name'] != "":
                        bRv = True

        self.assertEqual(bRv, True)

        # The category with the given ID does not exist.

    def test_02_getCategoriesNegTest404(self):
        status = self.getCategoryByID('blabla').status_code
        self.assertEqual(status, 404)

    def test_03_getCategoriesNegTest404(self):
        status = self.getParametersByCat('blabla').status_code
        self.assertEqual(status, 404)

    # @classmethod
    # def tearDown(self):
    #we should revoke token at the end

    def getCategoryByID(self, categoryId):
        res = requests.get(self.API_URL + "/sale/categories/" + categoryId, headers=self.HEADER)
        return res

    def getParametersByCat(self, categoryId):
        res = requests.get(self.API_URL + "/sale/categories/" + categoryId + "/parameters", headers=self.HEADER)
        return res


if __name__ == "__main__":
    if len(sys.argv) > 2:
        AllegroTest.SECRET = sys.argv.pop()
        AllegroTest.CLIENT = sys.argv.pop()
        unittest.main(failfast=True, exit=False)
