# REST API Allegro sample #

This project contains acceptation test for few methods of REST API Allegro.
Methods :
```bash
GET /sale/categories/
GET /sale/categories/{ID}
GET /sale/categories/{ID}/parameters
```
These methods are connected so I have connected in one positive test but I place in two negative tests.

### RUN: ###
Before running the program, you must register console application from:
["Click here to create new APP"](https://apps.developer.allegro.pl/new)

Save ClientID ClientSecret.

It needs python3.


```

Test.py ClientID ClientSecret
```

This will start a REST API test.

